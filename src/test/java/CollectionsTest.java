import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CollectionsTest extends TestCase {

    CollectionExercises collections = new CollectionExercises();

    public void testSum() {
        // calculate the total of all the values in the list
        List<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(6);
        list.add(19);
        list.add(4);
        list.add(12);
        list.add(6);

        Integer sum = collections.sumList(list);
        assertTrue(sum.equals(49));
    }

    public void testFilter1() {
        // filter out all the values lower than 5 from the list
        List<Integer> list = new ArrayList<Integer>();
        list.add(0);
        list.add(9);
        list.add(12);
        list.add(6);
        list.add(3);
        list.add(7);

        List<Integer> newList = collections.filterOutValuesLowerThan5(list);

        for (Integer i : newList) {
            assertTrue(i.compareTo(5) > 0);
        }
    }

    public void testFilter2() {
        // filter out all the odd numbers from the list
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(4);
        list.add(3);
        list.add(7);
        list.add(12);
        list.add(9);

        List<Integer> newList = collections.filterOutOddNumbers(list);

        for (Integer i : newList) {
            assertTrue(i % 2 == 0);
        }
    }

    public void testReverse() {
        // reverse
        List<String> list = new ArrayList<String>();
        list.add("Teen");
        list.add("Knie");
        list.add("Schouders");
        list.add("Hoofd");

        List<String> reversedList = collections.reverseList(list);
        assertTrue(reversedList.get(0).equals("Hoofd"));
        assertTrue(reversedList.get(3).equals("Teen"));

        // bonus points: considering List is a interface and not a
        // concrete class, could you swap it out for a different
        // collection type than ArrayList?
    }

    public void testSort() {
        // reverse
        List<Integer> list = new LinkedList<Integer>();
        list.add(5);
        list.add(9);
        list.add(0);
        list.add(4);

        List<Integer> reversedList = collections.sortList(list);
        assertTrue(reversedList.get(0).equals(0));
        assertTrue(reversedList.get(3).equals(9));

        // bonus points: considering List is a interface and not a
        // concrete class, could you swap it out for a different
        // collection type than LinkedList?
    }

    public void testApplesAndPears() {
        // this collection contains some duplicates.
        // which collection type can we use to enforce uniqueness of every element?
        int collectionSize = collections.applesAndPears().size();
        assertEquals(collectionSize, 2);
    }

    public void testSlowCollection() {
        // this function executes quite slow (~1000 ms)
        // can we use a different collection type to make it faster?
        long start = System.currentTimeMillis();
        List<Integer> result = collections.slowCollection();
        long end = System.currentTimeMillis();
        long executionTime = end - start;
        System.out.println("function executed in " + executionTime + " milliseconds");
        assertTrue(executionTime < 200);
    }

    public void testHashMap() {
        // this function should marshal the two input arrays into a HashMap representation
        // where the first array represents the keys and the second the values.
        // please provide the implementation
        String[] cities = { "Amsterdam", "Rotterdam", "Amersfoort", "Groningen" };
        Integer[] temperatures = { 20, 20, 19, 16 };
        Map<String, Integer> weatherData = collections.createHashMap(cities, temperatures);

        assertTrue(weatherData.get("Amsterdam").equals(20));
        assertTrue(weatherData.get("Rotterdam").equals(20));
        assertTrue(weatherData.get("Amersfoort").equals(19));
        assertTrue(weatherData.get("Groningen").equals(16));

        // bonus points: can you make the createHashMap function
        // generic over the input types (i.e. String & Integer)
    }

//    public void testSortStrings() {
//        // make the sortList method generic over the input
//        // type so it works with both Strings and Integers
//        List<String> list = new ArrayList<String>();
//        list.add("Zaandam");
//        list.add("Amersfoort");
//        list.add("Rotterdam");
//        list.add("Amsterdam");
//        list.add("Groningen");
//
//        // the following lines are commented out because
//        // the source needs to be changed before it will
//        // compile
//        List<String> reversedList = collections.sortList(list);
//        assertTrue(reversedList.get(0).equals("Amersfoort"));
//        assertTrue(reversedList.get(4).equals("Zaandam"));
//
//        // dummy test to force test to fail
//        assertTrue(false);
//    }
}